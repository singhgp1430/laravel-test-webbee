<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // created common function for response. 
    // somehow in future we need to add any new param in response.
    public function jsonResponseHelper($code,$json){
        return response()->json(['json'=>$json],$code);
    }    

}




//
